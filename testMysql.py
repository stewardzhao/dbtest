##!usr/bin/env python
## encoding:utf-8
 
 
'''
__Author__:沂水寒城
功能：使用pymssql连接SQLServer测试使用
'''
 
import sys
from importlib import reload
import logging
import pymssql
 
reload(sys)
#sys.setdefaultencoding("utf-8")
 
 
LOG_FORMAT="%(asctime)s - %(levelname)s - %(message)s"
DATE_FORMAT="%m-%d-%Y %H:%M:%S %p"
logging.basicConfig(filename='SQLServer.log',level=logging.DEBUG,format=LOG_FORMAT,
                    datefmt=DATE_FORMAT)
 
 
#数据库的；连接配置信息
config_dict={
            'user':'demo', #'你的用户名',
            'password':'DemoPassword', #'你的密码',
            'host':'127.0.0.1', #'你要连接的host地址',
            'database':'localdbv113'#'你要操作的数据库'
            }
 
 
 
tablename = "dsp_status" # DSP_STATUS #'你要操作的表名'
 
 
def SQLServer_connect(config_dict):
    '''
    SQLServer 数据库连接
    '''
    connect=pymssql.connect(**config_dict)
    print('Connect Successful!!!')
    return connect
 
 
def select_k_records(config_dict,tablename,topk=100):
    '''
    从SQLServer中选取前k条记录
    '''
    try: 
        connect=SQLServer_connect(config_dict)
        cursor=connect.cursor()  
        #统计记录数量
        result=[]
        cursor=connect.cursor() 
        select_sql='SELECT * FROM %s' %tablename
        print('select_sql is: ',select_sql) 
        cursor.execute(select_sql)
        row=cursor.fetchone()
        while row:
            if len(result)<topk:
                result.append(row)
                row=cursor.fetchone()
                print(row)
            else:
                break
        print('result: ')
        print(result)
        connect.close()
        cursor.close()
    except Exception as e:
        print("elect_sql error: " + e) 
    finally:
        connect.close()
    return result
 
 
def create_new_table(config_dict,tablename):
    '''
    创建表
    '''
    connect=SQLServer_connect(config_dict)
    cursor=connect.cursor() 
    #cursor.execute('select * into WZ_cgb_test from WZ_OA_PUBLICOPINION where 1=2')
    #省略号替换成自己的字段信息即可
    cursor.execute("""
    CREATE TABLE %s (
        id VARCHAR(32) NOT NULL,
        name VARCHAR(255) NULL,
        ......
        PRIMARY KEY(id)
    )""" %tablename
    )
    connect.commit()  #记得提交数据库事物操作
    connect.close()
    cursor.close()
 
 
def delete_record(config_dict,delete_sql):
    '''
    从 SQLServer 中删除数据记录
    '''
    try:
        connect=SQLServer_connect(config_dict)
        cursor=connect.cursor()  
        cursor.execute(delete_sql)
        connect.commit()
        print('DeleteOperation Finished!!!') 
    except Exception as e:
        print( "delete_sql error: " + e)
    finally:
        connect.close()
 
 
def count_records_num(config_dict,tablename):
    '''
    统计SQLServer中的数据记录数量
    '''
    try:
        result = []
        connect=SQLServer_connect(config_dict) 
        cursor=connect.cursor()
        cursor.execute('SELECT * FROM DSP_STATUS')
        result = cursor.fetchall()
        totalNum=cursor.rowcount
        print( 'Total Records Number is: ',str(totalNum))
        connect.close()
        cursor.close()
    except Exception as e:
        print("count_sql error: " , e)
    finally:
        connect.close()
    return len(result)
 
 
def insert_record(config_dict,insert_sql):
    '''
    向SQLServer中插入数据
    '''
    try:
        connect=SQLServer_connect(config_dict)
        cursor=connect.cursor() 
        cursor.execute(insert_sql)
        connect.commit()
        print('InsertOperation Finished!!!') 
    except Exception as e:
        print("insert_sql error: " + e) 
    finally:
        connect.close()
 
 
 
if __name__ == "__main__":
    count_records_num(config_dict,'dsp_status')
    #select_k_records(config_dict,tablename,topk=10)
 

