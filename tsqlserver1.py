#encoding: utf-8
from flask import Flask, escape, request
import config
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object(config)
db = SQLAlchemy(app)

db.create_all()

@app.route('/')
def hello():
    name = request.args.get("name", "Steward")
    return f'Hello, {escape(name)}!'
if __name__ == '__main__':
    app.run()