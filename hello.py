from flask import Flask, escape, request
import config

app = Flask(__name__)
app.config.from_object(config)

@app.route('/')
def hello():
    name = request.args.get("name", "Worldf-Hello")
    return f'Hello, {escape(name)}!'
if __name__ == '__main__':
    app.run()