#encoding: utf-8

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from myapp import app
from exts import db
from models import Article, students, User, Question, Answer
from db_manager import DBManager

manager = Manager(app)

#use flask_migrate must bind app and db
migrate = Migrate(app, db)

# add migratecommand into manager
manager.add_command('db', MigrateCommand)

# @manager.command
# def runserver():
#     print(u'服务器跑起来了！！！')


# 引入其他分manage的命令
# manager.add_command('db', DBManager) # 这里的‘db’在命令行操作时，是有用的


if __name__ == '__main__':
    manager.run()
