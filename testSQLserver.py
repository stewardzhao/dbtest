from flask import Flask, escape, request
import config
import pymssql

app = Flask(__name__)
app.config.from_object(config)


dbparams = {
            'user':'demo', #'你的用户名',
            'password':'DemoPassword', #'你的密码',
            'host':'qa1\\sql2017', #'你要连接的host地址',
            'database':'dbv115'#'你要操作的数据库'
            }
#print(app.config)

def SQLServer_connect(dbparams):

    conn = pymssql.connect(**dbparams)

    print('Connect Successful!!!')
    return conn

def querydata(sqlstr):
    conn = SQLServer_connect(dbparams)
    # 打开游标
    cur = conn.cursor();
    if not cur:
        raise Exception('数据库连接失败！')
    #sSQL = 'SELECT * FROM TB'
    #执行sql，获取所有数据
    cur.execute(sqlstr)
    result=cur.fetchall()
    #1.result是list，而其中的每个元素是 tuple
    print(type(result),type(result[0]))
    #2.
    print('\n\n总行数：'+ str(cur.rowcount))
    #3.通过enumerate返回行号
    #for i,(id,name,v) in enumerate(result):
    for i, row in enumerate(result):
      print('第 '+str(i+1)+' 行记录->>> ')
      for item in list(row):
          print(item)
      #str(id) +':'+ name+ ':' + str(v)

@app.route('/')
def hello():
    name = request.args.get("name", "Worldfh12rr3")
    return f'Hello, {escape(name)}!'
if __name__ == '__main__':
    sqlstr = 'select * from dsp_status'
    querydata(sqlstr)
    app.run()