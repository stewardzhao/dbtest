#encoding: utf-8
from exts import db
from datetime import datetime

class Article(db.Model):
    __tablename__ = 'article'
    id = db.Column(db.Integer, primary_key = True, autoincrement = True)
    title = db.Column(db.String(100), nullable = False)

    # def __init__(self, id, title):
    #     self.id = id
    #     self.title = title

class students(db.Model):
    __tablename__ = 'students'
    id = db.Column('student_id', db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    city = db.Column(db.String(50))  
    addr = db.Column(db.String(200))
    pin = db.Column(db.String(10))

    def __init__(self, name, city, addr,pin):
        self.name = name
        self.city = city
        self.addr = addr
        self.pin = pin

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True, autoincrement=True)
    telephone = db.Column(db.String(100))
    username = db.Column(db.String(50))
    password = db.Column(db.String(100))

class Question(db.Model):
    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key = True, autoincrement=True)
    title = db.Column(db.String(100))
    content = db.Column(db.Text)
    #now() means the server run time
    # now means every create a model, get the current time
    create_time = db.Column(db.DateTime, default=datetime.now)
    # foreign key
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    # relationship and backref
    author = db.relationship('User', backref=db.backref('questions'))

class Answer(db.Model):
    __tablename__ = 'answer'
    id = db.Column(db.Integer, primary_key = True, autoincrement=True)
    content = db.Column(db.Text)
    create_time = db.Column(db.DateTime, default=datetime.now)
    # foreign key
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))

    # relationship and backref
    question = db.relationship('Question', backref=db.backref('answers'))
    author = db.relationship('User', backref=db.backref('answers'))
