#encoding:utf8
from flask import Flask, session, url_for, redirect
from functools import wraps

app = Flask(__name__)


# limit login decorator
def login_required(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        if session.get('user_id'):
            return func(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrapper