#encoding:utf8
from flask import Flask, request, render_template, url_for, redirect, session
import config
from models import User, Question, Answer
from exts import db
from decorators import login_required
from sqlalchemy import desc


app = Flask(__name__)
app.config.from_object(config)
db.init_app(app) # 这个就是专为解决循环引用而出现的方法

@app.route('/')
def base():
    user_id = session.get('user_id')
    if user_id:
        user = User.query.filter(User.id == user_id).first()
        username = user.username
        return render_template('base.html', username=username)
    else:
        return render_template('base.html')

@app.route('/index/')
def index():
    content ={
        'questions': Question.query.order_by(desc('create_time')).all()
    }
    return render_template("index.html", **content)

@app.route('/question/', methods=['GET','POST'])
@login_required
def question():
    if request.method == 'GET':
        return render_template("question.html")
    else:
        title = request.form.get('title')
        content = request.form.get('content')
        question = Question(title = title, content = content)
        user_id = session.get('user_id')
        user = User.query.filter(User.id == user_id).first()
        question.author = user
        db.session.add(question)
        db.session.commit()
        return redirect(url_for('index'))

@app.route('/detail/<question_id>/')
@login_required
def detail(question_id):
    answer_content = {
        'question': Question.query.filter(Question.id == question_id).first(),
        'answers': Answer.query.filter(Answer.question_id == question_id).order_by(desc('create_time')).all(),
    }
    return render_template("detail.html",  **answer_content)

@app.route('/add_answer/', methods=['POST'])
def add_answer():
    content = request.form.get('answer_content')
    question_id = request.form.get('question_id')

    answer = Answer(content=content)
    user_id = session.get('user_id')
    user = User.query.filter(User.id == user_id).first()
    answer.author = user
    question = Question.query.filter(Question.id == question_id).first()
    answer.question = question
    db.session.add(answer)
    db.session.commit()

    return redirect(url_for('detail', question_id=question_id))

@app.route('/login/', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template("login.html")
    else:
        telephone = request.form.get('telephone')
        password = request.form.get('password')
        user = User.query.filter(User.telephone == telephone, User.password==password).first()
        if user:
            session['user_id'] = user.id
            # if want 31 days not login
            session.permanent = True
            return redirect(url_for('index'))
        else:
            return "telephone or password wrong!"

@app.route('/register/', methods=['GET','POST'])
def register():
    if request.method == 'GET':
        return render_template("register.html")
    else:
        telephone = request.form.get('telephone')
        username = request.form.get('username')
        password = request.form.get('password')
        password1 = request.form.get('password1')
        # confirm telephone, if it has been registered, can't register again
        user = User.query.filter(User.telephone == telephone).first()
        if user:
            return "The telephone has been registered, please change telephone!"
        else:
            # password == password1?
            if password!=password1:
                return "the two password not equal"
            else:
                user = User(telephone=telephone, username=username,password=password)
                db.session.add(user)
                db.session.commit()
                #if register successfully, redirect to index page
                return redirect(url_for('login'))

if __name__ == '__main__':
    app.run()