from flask import Flask, escape, request, render_template
from exts import db
import config
# from models import Article, students

app = Flask(__name__)
app.config.from_object(config)
db.init_app(app) # 这个就是专为解决循环引用而出现的方法

# push app to current_app # app.app_context().push() # if using config, must run this row.
# with app.app_context():  #if use migrate, don't use this command
#     db.create_all()

@app.route('/')
# def hello():
#     name = request.args.get("name", "Steward666")
#     return f'Hello, {escape(name)}!'

def show_all():
    # students = []
    return render_template('show_all.html', students = students.query.all() )

@app.route('/new/', methods = ['GET', 'POST'])
def new():
    if request.method == 'POST':
       if not request.form['name'] or not request.form['city'] or not request.form['addr']:
          flash('Please enter all the fields', 'error')
       else:
          student = students(request.form['name'], request.form['city'],request.form['addr'], request.form['pin'])
          print(student)
          db.session.add(student)
          db.session.commit()
          flash('Record was successfully added')
          return redirect(url_for('show_all'))
    return render_template('new.html')


if __name__ == '__main__':
    app.run()